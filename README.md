# Azuneed 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.

## Development server

Run `ng serve -c=sage` for a dev server environement Sage . Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
Run `ng serve -c=azuneed` for a dev server environement Azunned . Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build -c=sage ` to build the project with environement Sage. The build artifacts will be stored in the `frontend/` directory. Use the `--prod` flag for a production build.
Run `ng build -c=azuneed ` to build the project environement Azuneed. . The build artifacts will be stored in the `frontend/` directory. Use the `--prod` flag for a production build.

## Compte de test
email : aminesadfi1@gmail.com
pwd: amine55@
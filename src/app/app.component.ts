import { Component, OnInit, } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  title = 'Accueil'
  constructor(
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    let lang =  localStorage.getItem('lang')
    lang === null ? this.TranslateLang('en') :  this.TranslateLang(lang)   
  }
  TranslateLang(lang:string){
    this.translateService.setDefaultLang(lang);
    this.translateService.use(lang);
  }
}

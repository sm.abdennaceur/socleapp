import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState,
} from '@angular/cdk/layout';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, HostBinding, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Component({
  selector: 'layout-default',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  sideBarOpen = false;
  leftBarOpen = false;
  @HostBinding('class') className = '';

  
  constructor(private overlay: OverlayContainer) { }

  ngOnInit() {
  }

  sideBarToggler($event: any) {
    this.sideBarOpen = !this.sideBarOpen;
  }

  letBarToggler($event: any) {
    this.leftBarOpen = !this.leftBarOpen;
  }

  DarkModechecked($event: boolean) {
    const darkClassName = 'darkMode';
    this.className = $event ? darkClassName : '';
    if ($event) {
      this.overlay.getContainerElement().classList.add(darkClassName);
      document.documentElement.classList.add('dark')
    } else {
      this.overlay.getContainerElement().classList.remove(darkClassName);
      document.documentElement.classList.remove('dark')
    }

  }

}

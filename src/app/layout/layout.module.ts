import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LeftbarComponent } from './leftbar/leftbar.component';
import { HeaderComponent } from './header/header.component';
import { FlexLayoutModule } from '@angular/flex-layout';

import { LayoutComponent } from './layout.component';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';
import { SidebarModule } from 'ng-sidebar';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    LayoutComponent,
    SidebarComponent,
    LeftbarComponent,
    HeaderComponent, 
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    MaterialModule,
    SidebarModule,
    TranslateModule
  ],
  exports:[
    SidebarComponent,
    LeftbarComponent,
    HeaderComponent
  ]
})
export class LayoutModule { }

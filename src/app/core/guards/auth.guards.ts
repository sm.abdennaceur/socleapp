import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {AuthentificationService} from "../http/authentification.service";

@Injectable()
export class AuthGuard implements CanActivate{
    currentUser: any;
    constructor(private router: Router, private authenticationService: AuthentificationService) {
    }
    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.currentUser = this.authenticationService.currentUserValue;
        if (this.currentUser) {
            // logged in so return true
            return true;
        } else {
            // not logged in so redirect to login page with the return url
            this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
            return true;
        }
    }
}
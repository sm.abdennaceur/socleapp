import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { DynamicNavMenuComponent } from './dynamic-nav-menu/dynamic-nav-menu.component';
import { TranslateModule } from '@ngx-translate/core';
import { LoadingPlaceholderComponent } from './loading-placeholder/loading-placeholder.component';



@NgModule({
  declarations: [
    DynamicFormComponent,
    DynamicNavMenuComponent,
    LoadingPlaceholderComponent
  ],
  imports: [
    CommonModule,
    TranslateModule
  ],
  exports: [
    DynamicFormComponent,
    DynamicNavMenuComponent,
    LoadingPlaceholderComponent,
  ]
})
export class SharedModule { }

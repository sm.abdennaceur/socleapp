import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyCrewRoutingModule } from './my-crew-routing.module';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    MyCrewRoutingModule
  ]
})
export class MyCrewModule { }

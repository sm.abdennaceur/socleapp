import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyCompanyRoutingModule } from './my-company-routing.module';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    MyCompanyRoutingModule
  ]
})
export class MyCompanyModule { }

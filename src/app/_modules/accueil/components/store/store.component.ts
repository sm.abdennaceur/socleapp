import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StoryModalComponent } from '../story-modal/story-modal.component';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {

  @Input()ListStories:any;

  constructor(private modalService: NgbModal,) { }

  ngOnInit(): void {
  }

  openStoryModal() {
    this.modalService.open(StoryModalComponent);
  }
}

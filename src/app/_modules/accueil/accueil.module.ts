import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AccueilRoutingModule } from './accueil-routing.module';
 
import { MaterialModule } from 'src/app/material/material.module';
import { StoreComponent } from './components/store/store.component';
import { StoryModalComponent } from './components/story-modal/story-modal.component';
import { AccueilPage } from './page/accueil.page';
import { CardNewsComponent } from './components/card-news/card-news.component';
 
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '~shared/shared.module';

@NgModule({
  declarations: [
    AccueilPage,
    StoreComponent,
    StoryModalComponent,
    CardNewsComponent,
  ],
  imports: [
    NgbModule,
    CommonModule,
    AccueilRoutingModule,
    MaterialModule,
    TranslateModule,
    SharedModule
  ]
})
export class AccueilModule { }

import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-aide-page',
    template: `<mat-accordion>
  <mat-expansion-panel>
    <mat-expansion-panel-header>
      <mat-panel-title>
        <mat-icon class="material-icons">pages</mat-icon>
      </mat-panel-title>
      <mat-panel-description>
        Type your name and age
      </mat-panel-description>
    </mat-expansion-panel-header>

    <mat-form-field>
      <input matInput placeholder="First name">
    </mat-form-field>

    <mat-form-field>
      <input matInput placeholder="Age">
    </mat-form-field>
  </mat-expansion-panel> 

  <mat-expansion-panel>
    <mat-expansion-panel-header>
      <mat-panel-title>
        <i class="material-icons">navigation</i>
      </mat-panel-title>
      <mat-panel-description>
        Currently I am 
      </mat-panel-description>
    </mat-expansion-panel-header>
    <p>I'm visible because I am open</p>
  </mat-expansion-panel>
</mat-accordion>`,
    styles: [
        `
    mat-panel-title {
        flex-basis: 0;
      }
      mat-panel-description {
        flex-basis: 100%;
      }`
    ],
})

export class AidePage implements OnInit {
    ngOnInit(): void {
         
    }

}
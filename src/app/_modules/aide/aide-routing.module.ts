import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AidePage } from './page/aide.page';

const routes: Routes = [
  { path: '', component: AidePage,  data: {title: 'Help'} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AideRoutingModule { }

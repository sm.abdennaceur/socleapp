export const environment = {
  name:'azuneed',
  production: false,
  altImage:'dev',
  imageLogo : 'https://rh.azuneed.com/resources/img/azuneed_logo_removebg.png'
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/plugins/zone-error'; // Included with Angular CLI.

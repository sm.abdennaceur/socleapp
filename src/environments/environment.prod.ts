export const environment = {
  name:'production',
  production: true,
  altImage:'prod',
  imageLogo:'https://rh.azuneed.com/resources/img/azuneed_logo_removebg.png'
};

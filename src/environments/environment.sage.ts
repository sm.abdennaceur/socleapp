export const environment = {
    name:'sage',
    production: true,
    altImage:'sage',
    imageLogo:'https://espace-employes.sage.com/resources/other/logo%20sage.svg'
  };
  